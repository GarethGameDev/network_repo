﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class NetworkController : MonoBehaviourPunCallbacks {

	// Use this for initialization
	void Start () {
        PhotonNetwork.ConnectUsingSettings();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void OnConnectedToMaster()
    {
        //base.OnConnectedToMaster();
        Debug.Log("We are now connected to the " + PhotonNetwork.CloudRegion + " server!");
    }
}
