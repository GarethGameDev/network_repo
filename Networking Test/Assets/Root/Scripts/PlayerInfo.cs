﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : MonoBehaviour {

    public static PlayerInfo PlayInfo;
    public int mySelectedCharacter;
    public GameObject[] allCharacters;

    private void OnEnable()
    {
        if (PlayerInfo.PlayInfo == null)
        {
            PlayerInfo.PlayInfo = this;
        } else
        {
            if (PlayerInfo.PlayInfo != this)
            {
                Destroy(PlayerInfo.PlayInfo.gameObject);
                PlayerInfo.PlayInfo = this;
            }
        }
        DontDestroyOnLoad(this.gameObject);
    }

    // Use this for initialization
    void Start () {
		if (PlayerPrefs.HasKey("MyCharacter"))
        {
            mySelectedCharacter = PlayerPrefs.GetInt("MyCharacter");
        } else
        {
            mySelectedCharacter = 0;
            PlayerPrefs.SetInt("MyCharacter", mySelectedCharacter);
        }
	}
	
}
