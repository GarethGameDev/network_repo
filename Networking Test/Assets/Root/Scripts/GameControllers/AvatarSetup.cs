﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarSetup : MonoBehaviour {

    private PhotonView PV;
    public int characterValue;
    public GameObject myCharacter;

    public int playerHealth;
    public int playerDamage;

    public Camera myCamera;
    public AudioListener myAL;

    // Use this for initialization
    void Start () {
        PV = GetComponentInChildren<PhotonView>();

        //checks to see if we're the local player
        if (PV.IsMine)
        {
            //If we're the client then call the AddCharacter
            //RpcTarget.AllBuffered is used to allow players who joined after this has been sent to still receive it
            PV.RPC("RPC_AddCharacter", RpcTarget.AllBuffered, PlayerInfo.PlayInfo.mySelectedCharacter);
        }
        else
        {
            Destroy(myCamera);
            Destroy(myAL);
        }
	}
	
	[PunRPC]
    void RPC_AddCharacter(int whichCharacter)
    {
        characterValue = whichCharacter;
        myCharacter = Instantiate(PlayerInfo.PlayInfo.allCharacters[whichCharacter], transform.position, transform.rotation, transform);
    }

}
