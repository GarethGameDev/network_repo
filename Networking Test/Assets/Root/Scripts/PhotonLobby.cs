﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;


public class PhotonLobby : MonoBehaviourPunCallbacks {

    public static PhotonLobby lobby;

    public GameObject battleButton;
    public GameObject cancelButton;

    private void Awake()
    {
        lobby = this;

    }

    // Use this for initialization
    void Start () {
        //in the classic form of photon this method use to require passing a string that describes the version number of your game
        //This allowed incompatible versions of a game to be declared and made incapable of connecting to one another
        //This is now automated and found in the Unity Editor under the PhotonServerSettings (appversion)
        PhotonNetwork.ConnectUsingSettings();
	}

    public override void OnConnectedToMaster()
    {
        Debug.Log("Player has connected to the Photon Master Server");
        PhotonNetwork.AutomaticallySyncScene = true;
        battleButton.SetActive(true); 
    }

    //Called upon by the Battle Button in the UI scene
    public void OnBattleButtonClicked()
    {
        battleButton.SetActive(false);
        cancelButton.SetActive(true);
        PhotonNetwork.JoinRandomRoom();
    }
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Tried to join a random game but failed. There must be no open games available.");
        CreateRoom();
    }

    private void CreateRoom()
    {
        Debug.Log("Creating a new room.");
        int randomRoomName = Random.Range(0, 10000);
        RoomOptions roomOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = 10 };
        PhotonNetwork.CreateRoom("Room" + randomRoomName, roomOps);
    }

    //public override void OnJoinedRoom()
    //{
    //    Debug.Log("We are now in a room.");
    //}

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Tried to create a new room but failed, there must already be a room with the same name.");
        CreateRoom();
    }

    //Called upon by the Cancel Button in the UI scene
    public void OnCancelButtonClicked()
    {
        cancelButton.SetActive(false);
        battleButton.SetActive(true);
        PhotonNetwork.LeaveRoom();
    }
}
